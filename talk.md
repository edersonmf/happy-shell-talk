<!-- fg=white bg=black -->

# Hello shell my
# old friend!

## This is a talk providing some tips about how shell command line usage can increase
## productivity while being fun at the same time.

## Happiness is just around the corner.

By: Éderson Ferreira

---
<!-- fg=white bg=black -->

## What is a unix-like shell?

---
<!-- fg=white bg=black -->

## Z shell

- Automatic cd
- Recursive path expansion
- Command name spell checker
- Plugin and theme support

---
<!-- fg=white bg=black -->

## Commands

- is-up
- wikit
- cut
- sort
- uniq
- date
- sed
- xclip
- jq
- lsof
- netstat
- netcat
- base64
- tldr VS man
- aliases

---
<!-- fg=white bg=black -->

## For fun

- fortune
- cowsay
- lolcat

---

<!-- fg=white bg=black -->
<!-- effect=explosions -->

# Thank you!
# &
# You are welcome!

Deck available at https://gitlab.com/edersonmf/happy-shell-talk

---
<!-- fg=white bg=black -->

## If we have time

- tail
- head
- pgrep
- pkill
- fuser
