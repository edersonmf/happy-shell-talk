# Hello shell my old friend!

This is a talk providing some tips about how shell command line usage can increase productivity while being fun at the same time. Happiness is just around the corner.

## The hard way

### Install present

You can simply use pip to install `present`:

```bash
$ pip install present
```

### Usage

```bash
$ present talk.md
```

## The easy way

### Usage

```bash
$ curl https://gitlab.com/-/snippets/2021552/raw/master/talk.sh -o /tmp/talk.sh && sh /tmp/talk.sh
```